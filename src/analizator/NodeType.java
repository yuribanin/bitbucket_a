package analizator;

/**
 * Created by yury on 09/04/2019.
 */
public enum NodeType {
    UNDEFINED("<undefined>", false),
    OPERATOR_PLUS("+", false),
    OPERATOR_MINUS("-", false),
    OPERATOR_OR("OR",true),
    OPERATOR_AND("AND", true),
    OPERATOR_MUL("*", false),
    OPERATOR_DIV("/", false),
    UNARY_MINUS("-", false),
    OPERATOR_NOT(" NOT", true),
    OPERATOR_BETWEEN(" BETWEEN ", true),
    OPERATOR_NOTBETWEEN(" NOT BETWEEN ", true),
    OPERATOR_EQUAL("=", true),
    OPERATOR_NOT_EQUAL("!=", true),
    OPERATOR_GREATER(">", true),
    OPERATOR_LESS("<", true),
    OPERATOR_GE(">=", true),
    OPERATOR_LE("<=", true),
    OPERATOR_IN("IN", true),
    NUMBER_VALUE("", false),
    STRING_VALUE("", false),
    VARIABLE("", false),
    BOOLEAN_VALUE("BOOLEAN", false),
    ARRAY("", false);

    private String string;
    private boolean bool;

    NodeType(String s, boolean b) {
        this.string = s;
        this.bool = b;
    }

    public String getStringValue() {
        return string;
    }
    public boolean isBoolean() { return bool; }
}