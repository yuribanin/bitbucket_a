package analizator;

import java.util.LinkedList;
import java.util.List;

public class Main {

//    public static String where = "( (MD5=-123) OR CLASS_NAME=class345 OR CLASS_NAME=kjkj3333) AND NOT RQ_MESSAGE=566 OR RS_MESSAGE=HHHD333 AND TAG=656-22";
//    public static String where = "MD5 = 123 OR CLASS_NAME = class345 AND NOT RQ_MESSAGE = 566";
//    public static String where = "(( MD5 = 123.0 )OR(( CLASS_NAME = class345 )AND( NOT RQ_MESSAGE = 566.0 )))";
//    public static String where = "RS_MESSAGE=2333 AND NOT TAG='656-22'";
//    public static String where = "NOT RS_MESSAGE=2333 OR RS_MESSAGE=2333 AND NOT TAG='656-22' OR DD IN (1,3) AND IUk BETWEEN 1 AND 3";// AND SS IN (1,3,4)";

//    public static String where = "A = .001 OR C='25' AND IUk BETWEEN 1 AND 3 OR III IN (1,4)";

//    public static String where = "NOT RS_MESSAGE=2333.22 OR RS_MESSAGE=2333 AND NOT TAG='656-22' AND DD BETWEEN 1 AND 3 AND C IN (1,22) AND DD=111 AND U=23 OR C IN (1,44)";
public static String where = "A=1 AND B=2 AND c= 3 OR CCC=FALSE AND DDD = TRUE";
//    public static String where = "NOT(RS_MESSAGE=2333) OR RS_MESSAGE=2333 AND NOT TAG='656-22' OR DD BETWEEN 1 AND 3";
//    public static String where = "A=1 OR B=2 AND (NOT C=3 AND C1='22' AND C3='33' OR D=4)";

// NOT(RS_MESSAGE=?) OR ((RS_MESSAGE=?) AND  NOT(TAG=?) AND (DD BETWEEN ? AND ?) AND (C IN (?, ?)) AND (DD=?) AND (U=?) OR (C IN (?, ?)))
//    public static String where = "B BETWEEN 1 AND 2 AND C = 3";
//    public static String where = "(A=1 AND B=2 OR D=5) OR C=3";
//    public static String where = "((( A = 1 )AND( B = 2 )OR( D = 5 ))OR( C = 3 ))";
//    public static String where = "( NOT( RS_MESSAGE = 2333 )OR(( RS_MESSAGE = 2333 )AND NOT( TAG ='656-22')))";

//    public static String where2 = "( (MD5=-123) OR CLASS_NAME=class345 OR CLASS_NAME=kjkj3333)";
//    public static String where = "NOT RQ_MESSAGE = 566 OR RS_MESSAGE='HHHD333' AND TAG='656-22'";

//    public static String where = "(MD5 = '123'  OR O = 1 AND LL IN (1,2,3))";
//    public static String where = "(( MD5 ='123')OR(( O = 1 )AND( LL IN(  1 ,  2.0 ,  3.0   ))))";
//    public static String where ="(( MD5 ='123')OR(( O = -1 )AND( LL IN(  1 ,  2.0 ,  3.0  ))))";
//    public static String where ="(( MD5 ='123')OR(( O = -1.0 )AND( LL IN(  1.0 ,  2.0 ,  3.0  ))))";
    public static void main(String[] arg) {

        LogicalExpression.Node rootNode = null;

//        analizator.LogicalExpression expr = new analizator.LogicalExpression("(((MD5 = 123) OR (CLASS_NAME = class345)) AND EDD1 != NOT 566)");

        LogicalExpression expression = new LogicalExpression(where);

        System.out.println(where);

        rootNode = expression.createTree();
        expression.verifyTree(rootNode);

        expression.printTree();
        System.out.println();
        System.out.println(expression.getString(rootNode));
        System.out.println(expression.psValuesList);
        expression.clearTree();

//        List<JournalRequest.Condition> conditions = new LinkedList<JournalRequest.Condition>() {
//            {
//                add(new JournalRequest.Condition("RQ_MESSAGE", JournalRequest.Clause.EQ, "KIDDS563"));
//                add(new JournalRequest.Condition("MD5", JournalRequest.Clause.NOT_EQ, 123));
//                add(new JournalRequest.Condition("DD", JournalRequest.Clause.EQ, 534.22));
//                add(new JournalRequest.Condition("CLASS_NAME", JournalRequest.Clause.EQ, "lopfrt"));
//                add(new JournalRequest.Condition("DOTA", JournalRequest.Clause.EQ, "true"));
//            }
//        };
//
//        rootNode = expression.buildConditionTree(conditions);
//        expression.printTree(rootNode);
//        System.out.println(expression.getString(rootNode));
//        System.out.println(expression.psValuesList);
//        expression.verifyTree(rootNode);






        //TODO дописать селфчек на клаусы и возможность применения знаков =
        //TODO дописать сравнение имен переменных(полей) с метаданными + журнал проверять?

        //TODO DONE! дописать при сборке строки замену переменных на ? и складывание значений переменных в лист

        return;
    }
}