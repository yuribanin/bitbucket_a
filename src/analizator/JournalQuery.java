package analizator;

import java.util.List;

/**
 * Created by yury on 12/04/2019.
 */
public class JournalQuery {
    public final List<Field> fields;
    public final LogicalExpression.Node conditions;

    public JournalQuery(List<Field> fields, LogicalExpression.Node conditions) {
        this.fields = fields;
        this.conditions = conditions;
    }

    public static class Field {
        public String name;
    }
}
