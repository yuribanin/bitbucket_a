package analizator;

/**
 * Created by yury on 09/04/2019.
 */
public class Lexeme {
    public LexemeType type = LexemeType.ERROR;
    public double doubleValue = 0;
    public String stringValue = "";
    public String variable = "";
    public boolean booleanValue = false;
}