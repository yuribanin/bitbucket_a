package analizator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import static analizator.NodeType.*;

/**
 * Created by yury on 09/04/2019.
 * <p>
 * Класс для разбора и валидации логического выражения условия WHERE в SQL-запросе
 * <p>
 * Контекстно свободная грамматика. Абстрактно-синтаксическое дерево
 * Эволюция грамматической схемы, приоритеты операций:
 * S -> E EOL
 * E -> T | T 'OR' E
 * T -> L | L 'AND' T
 * L -> F | F '==' L | F '!=' L | F 'IN' '(' ARR ')' | F 'BETWEEN' BTWCOND
 * F -> NUMBER | VARIABLENAME | STRING | '(' E ')' | 'NOT/!' F | '-' F
 * ARR -> ARRFINAL | ARR ',' ARRFINAL
 * ARRFINAL -> NUMBER | VARIABLENAME | STRING
 * BTWCOND -> BTWFINAL 'AND' BTWFINAL
 * BTWFINAL -> NUMBER | STRING
 */
public class LogicalExpression {

    public static char EOL = 0;
    public static String placeHolder = "?";

    // хранилище переменных для prepared statement
    public List<Object> psValuesList = new LinkedList<>();

    private Node rootNode;
    private char[] expression;
    private int pos;
    private Lexeme currentLexeme = new Lexeme();

    private boolean isNumeric(String s) {
        int count = 0;
        for (char ch : s.toCharArray()) {
            if (ch == '.') {
                count++;
                continue;
            }
            if (!Character.isDigit(ch)) {
                return false;
            }
        }
        return count <= 1;
    }

    // парсим строку, которая может придти в значение переменной
    private boolean parseString(char startingSymbol) {
        if (expression[pos] == startingSymbol) {
            int endpos = pos + 1;
            while (expression[endpos] != startingSymbol) {
                if (expression[endpos] == EOL) {
                    throw new ParserException("Syntax error: Unterminated string. Position:" + pos);
                }
                endpos++;
            }
            String myStr = new String(expression, pos + 1, endpos - pos - 1);
            currentLexeme.type = LexemeType.STRING;
            currentLexeme.stringValue = myStr;
            System.out.println("GOT STRING: " + myStr);
            pos = endpos + 1;
            return true;
        }
        return false;
    }

    // parse next lexeme
    private void getNextLexeme() {
        // skip whitespaces
        while (expression[pos] == ' ') pos++;

        // end-of-line
        if (expression[pos] == EOL) {
            currentLexeme.type = LexemeType.END_OF_LINE;
            pos++;
            return;
        }

        if (parseString('\'')) return;
//        if (parseString(pos, '\"')) return;

        // ( ) , + - * / = == !=
        if (expression[pos] == '(') {
            currentLexeme.type = LexemeType.OPEN_PARENTHESIS;
            pos++;
            return;
        }
        if (expression[pos] == ')') {
            currentLexeme.type = LexemeType.CLOSED_PARENTHESIS;
            pos++;
            return;
        }
        if (expression[pos] == ',') {
            currentLexeme.type = LexemeType.COMMA;
            pos++;
            return;
        }
        if (expression[pos] == '+') {
            currentLexeme.type = LexemeType.PLUS;
            pos++;
            return;
        }
        if (expression[pos] == '-') {
            currentLexeme.type = LexemeType.MINUS;
            pos++;
            return;
        }
        if (expression[pos] == '*') {
            currentLexeme.type = LexemeType.MUL;
            pos++;
            return;
        }
        if (expression[pos] == '/') {
            currentLexeme.type = LexemeType.DIV;
            pos++;
            return;
        }
        if (expression[pos] == '=') {
            currentLexeme.type = LexemeType.EQUAL;
            pos++;
            if (expression[pos] == '=') {
                pos++;
            }
            return;
        }
        if (expression[pos] == '!') {
            // NOT-Equal
            if (expression[pos + 1] == '=') {
                currentLexeme.type = LexemeType.NOT_EQUAL;
                pos += 2;
                return;
            }
            // Single NOT operator
            currentLexeme.type = LexemeType.NOT;
            pos++;
            return;
        }

        // ищем идентификатор/значение/оператор/( , (конец лексемы)
        int endpos = pos + 1;
        while (expression[endpos] != EOL) {
            if (" ()-+*/!=,".indexOf(expression[endpos]) != -1) break;
            endpos++;
        }
        String identifier = new String(expression, pos, endpos - pos);
        // перемещаем текущую позицию
        pos = endpos;
        // AND/OR/IN/BETWEEN/NOT
        if ("AND".equals(identifier)) {
            currentLexeme.type = LexemeType.AND;
            return;
        }
        if ("OR".equals(identifier)) {
            currentLexeme.type = LexemeType.OR;
            return;
        }
        if ("IN".equals(identifier)) {
            currentLexeme.type = LexemeType.IN;
            return;
        }
        if ("BETWEEN".equals(identifier)) {
            currentLexeme.type = LexemeType.BETWEEN;
            return;
        }
        if ("NOT".equals(identifier)) {
            //TODO NOT BETWEEN case
            currentLexeme.type = LexemeType.NOT;
            return;
        }
        if ("TRUE".equals(identifier) || "FALSE".equals(identifier)) {
            currentLexeme.type = LexemeType.BOOLEAN;
            currentLexeme.booleanValue = Boolean.parseBoolean(identifier);
            return;
        }

        // NUMBER
        if (isNumeric(identifier)) {
            currentLexeme.type = LexemeType.NUMBER;
            currentLexeme.doubleValue = Double.parseDouble(identifier);
            return;
        }

        // тогда это переменная (last case)
        currentLexeme.type = LexemeType.VARIABLE;
        currentLexeme.variable = identifier;
    }

    // создание ноды для типа Операция
    private Node createNode(NodeType type, Node left, Node right) {
        Node node = new Node();
        node.type = type;
        node.left = left;
        node.right = right;
        return node;
    }

    // создание ноды для одиночной UNARY операции (like NOT)
    private Node createUnaryNode(NodeType type, Node right) {
        Node node = new Node();
        node.type = type;
        node.left = null;
        node.right = right;
        return node;
    }

    // создание ноды для значения (NUMBER)
    private Node createNodeNumber(double value) {
        Node node = new Node();
        node.type = NUMBER_VALUE;
        node.numberValue = value;
        return node;
    }

    // создание ноды для значения (STRING)
    private Node createNodeString(String value) {
        Node node = new Node();
        node.type = STRING_VALUE;
        node.stringValue = value;
        return node;
    }

    // создание ноды для списка (ARRAY)
    private Node createNodeArray() {
        Node node = new Node();
        node.type = NodeType.ARRAY;
        node.values.clear();
        return node;
    }

    // создание ноды для переменной (VARIABLE)
    private Node createNodeVariable(String varname) {
        Node node = new Node();
        node.type = NodeType.VARIABLE;
        node.variableName = varname;
        return node;
    }

    private Node createNodeBoolean(boolean value) {
        Node node = new Node();
        node.type = NodeType.BOOLEAN_VALUE;
        node.booleanValue = value;
        return node;
    }

    // START
    private Node parseS() {
        Node node = parseE();
        if (currentLexeme.type != LexemeType.END_OF_LINE) {
            throw new ParserException("Syntax Error: End of line expected. Position:" + pos);
        }
        return node;
    }

    // OR
    private Node parseE() {
        Node node = parseT();
        if (currentLexeme.type == LexemeType.OR) {
            getNextLexeme();
            node = createNode(NodeType.OPERATOR_OR, node, parseE());
        }
        return node;
    }

    // AND
    private Node parseT() {
        Node node = parseL();
        if (currentLexeme.type == LexemeType.AND) {
            getNextLexeme();
            node = createNode(NodeType.OPERATOR_AND, node, parseT());
        }
        return node;
    }

    // == , !=, IN(array parser), BETWEEN
    private Node parseL() {
        Node node = parseF();
        if (currentLexeme.type == LexemeType.EQUAL) {
            getNextLexeme();
            node = createNode(NodeType.OPERATOR_EQUAL, node, parseL());
        } else if (currentLexeme.type == LexemeType.NOT_EQUAL) {
            getNextLexeme();
            node = createNode(NodeType.OPERATOR_NOT_EQUAL, node, parseL());
        } else if (currentLexeme.type == LexemeType.IN) {
            getNextLexeme();
            if (currentLexeme.type != LexemeType.OPEN_PARENTHESIS) {
                throw new ParserException("Syntax error: ( missing. Position:" + pos);
            }
            getNextLexeme();
            node = createNode(NodeType.OPERATOR_IN, node, parseArr(null));
            if (currentLexeme.type != LexemeType.CLOSED_PARENTHESIS) {
                throw new ParserException("Syntax error: ) missing. Position:" + pos);
            }
            getNextLexeme();
        } else {
            //BETWEEN parser:
            if (currentLexeme.type == LexemeType.BETWEEN) {
                getNextLexeme();
                node = createNode(NodeType.OPERATOR_BETWEEN, node, parseBTWCond());
            }
        }
        return node;
    }

    // VARIABLE, NUMBER, STRING, BOOLEAN, (, ), NOT, UNARYMINUS:
    private Node parseF() {
        Node node;
        if (currentLexeme.type == LexemeType.NUMBER) {
            node = createNodeNumber(currentLexeme.doubleValue);
            getNextLexeme();
        } else if (currentLexeme.type == LexemeType.VARIABLE) {
            node = createNodeVariable(currentLexeme.variable);
            getNextLexeme();
        } else if (currentLexeme.type == LexemeType.BOOLEAN) {
            node = createNodeBoolean(currentLexeme.booleanValue);
            getNextLexeme();
        } else if (currentLexeme.type == LexemeType.STRING) {
            node = createNodeString(currentLexeme.stringValue);
            getNextLexeme();
        } else if (currentLexeme.type == LexemeType.NOT) {
            getNextLexeme();
            node = createUnaryNode(NodeType.OPERATOR_NOT, parseL());
        } else if (currentLexeme.type == LexemeType.MINUS) {
            getNextLexeme();
            node = createUnaryNode(NodeType.UNARY_MINUS, parseF());
        } else if (currentLexeme.type == LexemeType.OPEN_PARENTHESIS) {
            getNextLexeme();
            node = parseE();
            if (currentLexeme.type != LexemeType.CLOSED_PARENTHESIS) {
                throw new ParserException("Syntax Error: ) missing. Position:" + pos);
            }
            getNextLexeme();
        } else {
            throw new ParserException("Syntax Error: Value, Variable, (, NOT expected. Position:" + pos);
        }

        return node;
    }

    // ARR    массив IN (, , , )
    private Node parseArr(Node node) {
        if (node == null) {
            node = createNodeArray();
        }
        parseArrFinal(node);
        if (currentLexeme.type == LexemeType.COMMA) {
            System.out.println("GOT COMMA");
            getNextLexeme();
            node = parseArr(node);
        }
        return node;
    }


    // ARRFINAL: VARIABLE, NUMBER, STRING in ARRAY only
    private Node parseArrFinal(Node node) {
        if (currentLexeme.type == LexemeType.NUMBER) {
            node.values.add(createNodeNumber(currentLexeme.doubleValue));
            getNextLexeme();
        } else if (currentLexeme.type == LexemeType.VARIABLE) {
            node.values.add(createNodeVariable(currentLexeme.variable));
            getNextLexeme();
        } else if (currentLexeme.type == LexemeType.STRING) {
            node.values.add(createNodeString(currentLexeme.stringValue));
            getNextLexeme();
        } else {
            throw new ParserException("Syntax Error: Value, Variable, String expected in ARRAY. Position:" + pos);
        }
        return node;
    }

    // BETWEEN condition
    private Node parseBTWCond() {
        Node lnode = parseBTWFinal();
        //BETWEEN must contain AND only
        if (currentLexeme.type != LexemeType.AND) {
            throw new ParserException("Syntax Error: BETWEEN operator must contain only AND condition. Position:" + pos);
        }
        getNextLexeme();
        Node rnode = parseBTWFinal();
        return createNode(NodeType.OPERATOR_AND, lnode, rnode);
    }

    // BTWFINAL
    private Node parseBTWFinal() {
        Node node = null;
        if (currentLexeme.type == LexemeType.NUMBER) {
            node = createNodeNumber(currentLexeme.doubleValue);
            getNextLexeme();
        } else if (currentLexeme.type == LexemeType.STRING) {
            node = createNodeString(currentLexeme.stringValue);
            getNextLexeme();
        } else {
            throw new ParserException("Syntax Error: Value, String expected in BETWEEN condition. Position:" + pos);
        }
        return node;
    }

    public LogicalExpression(String str) {
        char[] temp = str.toCharArray();
        this.expression = new char[temp.length + 1];
        System.arraycopy(temp, 0, this.expression, 0, temp.length);
        this.expression[temp.length] = 0;

        rootNode = null;
    }

    // парсим строку и создаем дерево
    public Node createTree() {
        pos = 0;
        getNextLexeme();
        rootNode = parseS();
        return rootNode;
    }

    public void clearTree() {
        clearTreeRecursive(rootNode);
        rootNode = null;
    }

    private void clearTreeRecursive(Node node) {
        if (node == null) return;
        clearTreeRecursive(node.left);
        clearTreeRecursive(node.right);
//        System.out.println("Delete Node: " + node.type);
        node = null;
    }

    public String getString(Node node) {
        return printExpression(node);
    }

    private String printExpression(Node node) {
        if (node == null) {
            return "";
        }
        String psString = "";
        boolean needParenthese = true;
        boolean needQuote = false;
        boolean isOperand = NUMBER_VALUE.equals(node.type) || STRING_VALUE.equals(node.type) || BOOLEAN_VALUE.equals(node.type);
        if (isOperand || node.type == VARIABLE || node.type == UNARY_MINUS || node.type == OPERATOR_NOT || node.type == OPERATOR_AND || node.type == BOOLEAN_VALUE) {
            needParenthese = false;
        }
        if (node.type == STRING_VALUE) {
            needQuote = true;
        }
        psString = needParenthese ? "(" : "";
        psString += needQuote && !isOperand ? "'" : "";
        psString += printExpression(node.left);
        // for PreparedStatement string with '?' placeholders
        if (isOperand) {
            psString = psString + placeHolder;
            psValuesList.add(NUMBER_VALUE.equals(node.type) ? formatNumberTypeForPS(node.numberValue) : BOOLEAN_VALUE.equals(node.type) ? node.booleanValue : node.stringValue);
        } else {
            psString = psString + (ARRAY.equals(node.type) ? node.getPrintableListValue(true) : node.toString());
        }
        psString += printExpression(node.right);
        psString += needParenthese ? ")" : "";
        psString += needQuote && !isOperand ? "'" : "";
        return psString;
    }

    public static Object formatNumberTypeForPS(double d) {
        if (d == (long) d) {
            return Long.valueOf(String.format("%d", (long) d));
        } else {
            return Double.valueOf(String.format("%s", d));
        }
    }

//   TODO (NOT RS_MESSAGE=2333 OR RS_MESSAGE=2333 AND NOT TAG='656-22' OR DD BETWEEN 1 AND 3

    public void printTree() {
        if (rootNode != null)
            printTreeInternal("", rootNode, false);
    }

    public void printTree(Node node) {
        printTreeInternal("", node, false);
    }

    private void printTreeInternal(String prefix, Node node, boolean isLeft) {
        if (node != null) {
            System.out.print(prefix);
            System.out.print(isLeft ? "|--" : "+--");

            switch (node.type) {
                case NUMBER_VALUE:
                    System.out.println("NUMBER VALUE: " + node.numberValue);
                    break;
                case VARIABLE:
                    System.out.println("VARIABLE: " + node.variableName);
                    break;
                case STRING_VALUE:
                    System.out.println("STRING VALUE: " + node.stringValue); //TODO попробовать toString и выше тоже
                    break;
                case OPERATOR_OR:
                    System.out.println("OR");
                    break;
                case OPERATOR_BETWEEN:
                    System.out.println("BETWEEN");
                    break;
                case OPERATOR_NOTBETWEEN:
                    System.out.println("NOTBETWEEN");
                    break;
                case OPERATOR_IN:
                    System.out.println("IN");
                    break;
                case OPERATOR_AND:
                    System.out.println("AND");
                    break;
                case OPERATOR_NOT:
                    System.out.println("NOT");
                    break;
                case OPERATOR_EQUAL:
                    System.out.println("EQUAL");
                    break;
                case OPERATOR_NOT_EQUAL:
                    System.out.println("NOT_EQUAL");
                    break;
                case UNARY_MINUS:
                    System.out.println(" - ");
                    break;
                case ARRAY:
                    System.out.println("ARRAY: " + node.toString());
                    break;
                case BOOLEAN_VALUE:
                    System.out.println("BOOLEAN VALUE: " + node.booleanValue.toString().toUpperCase());
                    break;
                default:
                    System.out.println("???");
                    break;
            }
            printTreeInternal(prefix + (isLeft ? "|   " : "    "), node.left, true);
            printTreeInternal(prefix + (isLeft ? "|   " : "    "), node.right, false);
        }
    }

    public void verifyTree(Node node) {
        verifyTreeRecursive(node);
    }

    private void verifyTreeRecursive(Node node) {
        if (node == null) return;
        node.selfCheck();
        verifyTreeRecursive(node.left);
        verifyTreeRecursive(node.right);
    }

    public Node buildConditionTree(List<JournalRequest.Condition> conditionList) {
        Iterator<JournalRequest.Condition> iterator = conditionList.iterator();
        return createConditionNode(OPERATOR_AND, iterator);
    }

    private Node createConditionNode(NodeType nodeType, Iterator<JournalRequest.Condition> iterator) {
        if (!iterator.hasNext()) return null;
        Node node = new Node();
        JournalRequest.Condition condition = iterator.next();
        if (iterator.hasNext()) { // тогда тип операции AND(переданный nodeType)
            node.type = nodeType;
            node.left = createNode(getOperatorType(condition.clause), getFieldNode(condition), getValueNode(condition));
            node.right = createConditionNode(nodeType, iterator);
        } else {
            node = createNode(getOperatorType(condition.clause), getFieldNode(condition), getValueNode(condition));
        }
        return node;
    }

    private Node getFieldNode(JournalRequest.Condition condition) {
        Node node = new Node();
        node.type = NodeType.VARIABLE;
        node.variableName = condition.field;
        return node;
    }

    private Node getValueNode(JournalRequest.Condition condition) {
        Node node = new Node();
        if (condition.value instanceof String) {
            node.type = NodeType.STRING_VALUE;
            node.stringValue = (String) condition.value;
        } else if (condition.value instanceof Integer) {
            node.type = NodeType.NUMBER_VALUE;
            node.numberValue = (Integer) condition.value;
        } else if (condition.value instanceof Double) {
            node.type = NodeType.NUMBER_VALUE;
            node.numberValue = (Double) condition.value;
        }
        return node;
    }

    private NodeType getOperatorType(JournalRequest.Clause clause) {
        switch (clause) {
            case EQ:
                return OPERATOR_EQUAL;
            case NOT_EQ:
                return OPERATOR_NOT_EQUAL;
            default:
                return UNDEFINED;
        }
    }

    public class Node {
        public NodeType type;
        public double numberValue;
        public String stringValue;
        public String variableName;
        public Boolean booleanValue;
        public List<Node> values;
        public Node left;
        public Node right;

        public Node() {
            variableName = "";
            type = NodeType.UNDEFINED;
            numberValue = 0;
            stringValue = "";
            booleanValue = false;
            values = new ArrayList<>();
            left = null;
            right = null;
        }

        @Override
        public String toString() {
            if (NodeType.NUMBER_VALUE.equals(type)) {
                return String.valueOf(numberValue);
            } else if (NodeType.STRING_VALUE.equals(type)) {
                return stringValue;
            } else if (NodeType.VARIABLE.equals(type)) {
                return variableName;
            } else if (NodeType.BOOLEAN_VALUE.equals(type)) {
                return booleanValue.toString();
            } else if (NodeType.ARRAY.equals(type)) {
                return " " + getPrintableListValue(false) + " ";
            } else if (NodeType.OPERATOR_AND.equals(type) || NodeType.OPERATOR_OR.equals(type) || NodeType.OPERATOR_IN.equals(type)) {
                return " " + type.getStringValue() + " ";
            } else {
                return type.getStringValue();
            }
        }

        public String getPrintableListValue(boolean forPreparedStatement) {
            String logicalExpressionString = "";
            String placeHolder = "?";
            int count = values.size();
            for (Node element : values) {
                count--;
                logicalExpressionString += (forPreparedStatement ? placeHolder : element.toString()) + (count != 0 ? ", " : "");
                if (forPreparedStatement) {
                    if (NodeType.NUMBER_VALUE.equals(element.type)) {
                        psValuesList.add(LogicalExpression.formatNumberTypeForPS(element.numberValue));
                    } else if (NodeType.STRING_VALUE.equals(element.type)) {
                        psValuesList.add(element.stringValue);
                    }
                }
            }
            return logicalExpressionString;
        }

        public boolean selfCheck() {
            switch (type) {
                case OPERATOR_BETWEEN:
                    return selfCheckBetween();
                case OPERATOR_NOTBETWEEN:
                    return selfCheckBetween();
                case OPERATOR_EQUAL:
                    return selfCheckEqual();
                case OPERATOR_NOT_EQUAL:
                    return selfCheckEqual();
                case UNARY_MINUS:
                    return selfCheckUnaryMinus();
            }
            return true;
        }

        private boolean selfCheckBetween() {
            if (left.type != NodeType.VARIABLE) {
                throw new ParserException("Lvalue for BETWEEN operator must be a variable." + left);
            }
            if (right.type != NodeType.OPERATOR_AND) {
                throw new ParserException("Rvalue for BETWEEN operator must be AND. " + right );
            }
            //правый узел у BETWEEN содержит узел AND
            if (
                    !(
                            (right.left.type == NodeType.NUMBER_VALUE || right.left.type != NodeType.STRING_VALUE) &&
                                    (right.right.type == NodeType.NUMBER_VALUE || right.right.type == NodeType.STRING_VALUE)
                    )
            ) {
                throw new ParserException("AND for BETWEEN operator must contain only String or Number constants");
            }
            // BETWEEN только между двумя значениями
            if (!(right.left.left == null && right.left.right == null && right.right.left == null && right.right.right == null)) {
                throw new ParserException("AND node in BETWEEN operator must be between only two values");
            }
            return true;
        }

        // проверка оператора EQUAL - левое значение должно быть переменная, правое значение - константа
        private boolean selfCheckEqual() {
            if (left.type != NodeType.VARIABLE && left.type != NodeType.OPERATOR_NOT) {
                throw new ParserException("Lvalue for EQUAL operator must be variable. " + left);
            }
            if (right.type != NodeType.STRING_VALUE && right.type != NodeType.NUMBER_VALUE && right.type != NodeType.BOOLEAN_VALUE && right.type != NodeType.UNARY_MINUS) {
                throw new ParserException("Rvalue for EQUAL operator must be String or Number. " + right );
            }
            return true;
        }

        //правое значение унарного минуса должно быть число
        private boolean selfCheckUnaryMinus() {
            if (right.type != NodeType.NUMBER_VALUE && right.type != NodeType.UNARY_MINUS) {
                throw new ParserException("Rvalue for UNARY minus must be Number or another Unary minus. " + right);
            }
            return true;
        }
    }

}