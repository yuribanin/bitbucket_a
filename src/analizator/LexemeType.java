package analizator;

/**
 * Created by yury on 09/04/2019.
 */
public enum LexemeType {
    ERROR,
    PLUS,
    MINUS,
    MUL,
    DIV,
    OR,
    BETWEEN,
    AND,
    NOT,
    EQUAL,
    NOT_EQUAL,
    IN,
    BOOLEAN,
    END_OF_LINE,
    COMMA,
    OPEN_PARENTHESIS,
    CLOSED_PARENTHESIS,
    NUMBER,
    STRING,
    GREATER,
    LESS,
    GE,
    LE,
    VARIABLE
}