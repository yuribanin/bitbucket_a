package analizator;

import java.util.List;

/**
 * Created by yury on 12/04/2019.
 */
public class JournalRequest {
    public final String code;
    public final List<String> fields;
    public final List<Condition> condition;

    public JournalRequest(String code, List<String> fields, List<Condition> condition) {
        this.code = code;
        this.fields = fields;
        this.condition = condition;
    }

    public static class Condition {
        public final String field;
        public final Clause clause;
        public final Object value;

        public Condition(String field, Clause clause, Object value) {
            this.field = field;
            this.clause = clause;
            this.value = value;
        }
    }

    public enum Clause {
        EQ(" = "), NOT_EQ(" != "), LIKE(" LIKE ");
        String string;
        Clause(String s) {
            this.string = s;
        }
        public String getString() {
            return string;
        }
    }
}
