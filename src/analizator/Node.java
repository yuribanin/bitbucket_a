//package analizator;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class Node {
//    public NodeType type;
//    public double numberValue;
//    public String stringValue;
//    public String variableName;
//    public List<Node> values;
//    public Node left;
//    public Node right;
//
//    public Node() {
//        variableName = "";
//        type = NodeType.UNDEFINED;
//        numberValue = 0;
//        stringValue = "";
//        values = new ArrayList<>();
//        left = null;
//        right = null;
//    }
//
//    @Override
//    public String toString() {
//        if (NodeType.NUMBER_VALUE.equals(type)) {
//            return String.valueOf(numberValue);
//        } else if (NodeType.STRING_VALUE.equals(type)) {
//            return stringValue;
//        } else if (NodeType.VARIABLE.equals(type)) {
//            return variableName;
//        } else if (NodeType.ARRAY.equals(type)) {
//            return " " + getPrintableListValue(false) + " ";
//        } else if (NodeType.OPERATOR_AND.equals(type) || NodeType.OPERATOR_OR.equals(type) || NodeType.OPERATOR_IN.equals(type)) {
//            return " " + type.getStringValue() + " ";
//        } else {
//            return type.getStringValue();
//        }
//    }
//
//    public String getPrintableListValue(boolean forPreparedStatement) {
//        String logicalExpressionString = "";
//        String placeHolder = "?";
//        int count = values.size();
//        for (Node element : values) {
//            count--;
//            logicalExpressionString += (forPreparedStatement ? placeHolder : element.toString()) + (count != 0 ? ", " : "");
//            if (forPreparedStatement) {
//                if (NodeType.NUMBER_VALUE.equals(element.type)) {
//                    psValuesList.add(LogicalExpression.formatNumberTypeForPS(element.numberValue));
//                } else if (NodeType.STRING_VALUE.equals(element.type)) {
//                    psValuesList.add(element.stringValue);
//                }
//            }
//        }
//        return logicalExpressionString;
//    }
//
//    public boolean selfCheck() {
//        switch (type) {
//            case OPERATOR_BETWEEN:
//                return selfCheckBetween();
//            case OPERATOR_NOTBETWEEN:
//                return selfCheckBetween();
//            case OPERATOR_EQUAL:
//                return selfCheckEqual();
//            case OPERATOR_NOT_EQUAL:
//                return selfCheckEqual();
//            case UNARY_MINUS:
//                return selfCheckUnaryMinus();
//        }
//        return true;
//    }
//
//    private boolean selfCheckBetween() {
//        if (left.type != NodeType.VARIABLE) {
//            throw new ParserException("Lvalue for BETWEEN operator must be a variable." + left);
//        }
//        if (right.type != NodeType.OPERATOR_AND) {
//            throw new ParserException("Rvalue for BETWEEN operator must be AND. " + right );
//        }
//        //правый узел у BETWEEN содержит узел AND
//        if (
//                !(
//                        (right.left.type == NodeType.NUMBER_VALUE || right.left.type != NodeType.STRING_VALUE) &&
//                                (right.right.type == NodeType.NUMBER_VALUE || right.right.type == NodeType.STRING_VALUE)
//                )
//        ) {
//            throw new ParserException("AND for BETWEEN operator must contain only String or Number constants");
//        }
//        // BETWEEN только между двумя значениями
//        if (!(right.left.left == null && right.left.right == null && right.right.left == null && right.right.right == null)) {
//            throw new ParserException("AND node in BETWEEN operator must be between only two values");
//        }
//        return true;
//    }
//
//    // проверка оператора EQUAL - левое значение должно быть переменная, правое значение - константа
//    private boolean selfCheckEqual() {
//        if (left.type != NodeType.VARIABLE && left.type != NodeType.OPERATOR_NOT) {
//            throw new ParserException("Lvalue for EQUAL operator must be variable. " + left);
//        }
//        if (right.type != NodeType.STRING_VALUE && right.type != NodeType.NUMBER_VALUE && right.type != NodeType.UNARY_MINUS) {
//            throw new ParserException("Rvalue for EQUAL operator must be String or Number. " + right );
//        }
//        return true;
//    }
//
//    //правое значение унарного минуса должно быть число
//    private boolean selfCheckUnaryMinus() {
//        if (right.type != NodeType.NUMBER_VALUE && right.type != NodeType.UNARY_MINUS) {
//            throw new ParserException("Rvalue for UNARY minus must be Number or another Unary minus. " + right);
//        }
//        return true;
//    }
//}
